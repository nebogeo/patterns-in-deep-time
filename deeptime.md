---
# This template is licensed under a Creative Commons 0 1.0 Universal License (CC0 1.0). Public Domain Dedication.

title: 'Patterns in deep time'
author:
  - name: Dave Griffiths
    affiliation: Then Try This
    email: dave@thentrythis.org
  - name: Elizabeth Wilson
    affiliation: Queen Mary, University of London
    email: ewilson01@protonmail.ch
  - name: Iván Paz
    affiliation: Toplap Barcelona
    email: ivan@toplap.cat
  - name: Alex M^c^Lean
    affiliation: Then Try This
    email: alex@slab.org
  - name: Joana Chicau
    affiliation: 
    email: web@joanachicau.com
  - name: Flor de Fuego
    affiliation:
    email: floralonsoflor@gmail.com
  - name: Timo Hoogland
    affiliation: HKU University of the Arts, Utrecht
    email: info@timohoogland.com
  - name: Eloi Isern
    affiliation:
    email: nuntxaku@gmail.com
  - name: Michael-Jon Mizra
    affiliation:
    email: info@mizra.co.uk
  - name: Roger Pibernat
    affiliation:
    email: rogerpibernat@gmail.com
abstract: |
  In this paper, we look to ground how physical pattern making can be a useful activity for live coders used to the digital realm.  We ran an algorithmic patterns workshop in July 2022 – with a node at on_the_fly.collect(_) festival in Barcelona, a node in Sheffield and the workshop leader in Penryn – where we created an activity recreating ancient patterns by weaving on tablet looms that we constructed from card and yarn, and sent to the participants for this remote/multi location workshop. One of the aims of the Alpaca project is to highlight the relationship people have had with patterns over history, and how we can use this to uncover certain misconceptions we have about algorithmic patterns in contemporary society. We collected responses from those who participated in the workshop and collated the responses. We found that tablet weaving allows us to connect the physical patterns with their algorithmic descriptions. Also, errors relate with the trade off among expectations and surprise and exploring new unexpected possibilities. Finally, sharing the experiences among the participants allows us to observe how we interpret patterns when comparing it with others experiences.
fontsize: 11pt
geometry: margin=2cm
fontfamily: libertine
fontfamily: inconsolata
mainfont: Linux Libertine O
monofont: Inconsolata
bibliography: references.bib
...

# Introduction

This paper introduces a workshop which explored algorithmic patterns [@mcleanAlgorithmicPattern2020] across both textiles and live coding.^[For more on the algorithmic pattern research theme, see [https://algorithmicpattern.org/](algorithmicpattern.org).] First, we will share background to the thinking that led up to this workshop, providing cultural and historical reference points. Then we will share experiences and reflections as workshop participants, and conclude with further reflections on where this line of research will end up next.

The workshop took place in hybrid format in July 2022 split over two locations – with a node at the on_the_fly.collect(_) festival based at the Hangar.org space in Barcelona, and another node based in Sheffield. The workshop was convened by Iván Paz and Lizzie (Elizabeth) Wilson in Barcelona, and Alex McLean in Sheffield, with the support of the rest of the on-the-fly project team including Ludovica Michelin and Lina Bautista.

# Digital vs physical

The workshop was led by Dave Griffiths, based on his personal experience of learning weaving and programming at the same time when very young. This early foundation provided a certain way of seeing the parallels between these digital worlds - not in analogy or on the surface level, but providing two views on the same underlying cosmos. 

Both weaving and programming involve the exploration of entangled, discrete structures. So to set the scene, we discussed the meeting between the UK prime minister and advisors when rapidly deciding policy in the days leading up to the first COVID lockdown. In a space devoid of internet access, except one smartphone (belonging to Dominic Cummings) , they were desperately trying to understand all the issues involved on a single white board.

![The whiteboard used to plan the UK government’s initial covid response on 13 March 2020](images/whiteboard.png){width=80%}

What this extreme case exhibits is that while we have access to vast amounts of information, none of it is truly trusted in an emergency situation. This can be seen in both the lack of provision of internet access in such buildings (where this decision making needs to happen) but also in the reluctance to follow evidence unless something urgent enough happens to suddenly “connect” it to the “real world” - e.g. the sudden realisation that “abstract” model predictions based on their decisions would affect hospital admissions in a very real way.

That on a certain level we don’t really believe that data is real, is perhaps due to the fact we have split the world into two entities: the grounded, trustworthy world of “the physical”: objects that can be touched, shaped by our hands and passed to other people, regulated by a scarcity which appeals to our feelings of simplicity, and knowing right from wrong. The other world is “the digital”: objects that we can only grasp indirectly, concepts and structures of abundance - most likely creations working in the service of large multinational companies. These digital objects are generally understood as new, suspect and deeply untrustworthy.

# Weaving

Weaving breaks this false dichotomy in ways that make it possible to more effectively critique the digital infrastructure we inhabit. Threads are digital in precisely the same way voltages in our smartphones are digital - we combine these discrete elements into patterns we can use. This is not merely an analogy, but a tangible reality, which can for example be seen in how a woven artefact is a digital representation of its own making. The discrete structure of a weave can be replicated exactly as a digital signal sent in physical form via textile, exhibiting the same properties as a digital signal sent via radio waves. A woven textile can pass through long distances (of time) and be read perfectly when it reaches its destination.


![Section of the Hallstatt tablet weaving circa 800-400 BC in our tablet weaving simulator, with code, tablet rotations and pattern shown.](images/hallstatt.png){width=80%}

As an example of weaving signals travelling through time, archaeologists 'read' pieces of fabric such as the Hallstatt tablet weaving discovered preserved in an Iron age salt mine, and then convert such data to an intermediate code that records the weaving tablet/card turning movements of the weaver from three thousand years ago. This code may then be followed to recreate the fabric. The recreated version will include all the micro-decisions and indeed mistakes of the original weaver. In this way, we can see that weaving looms are digital tools. They have passed through many more hands than the silicon-based digital tools we are more used to thinking about, and this perspective reveals how the human relationship with digital thinking goes back to prehistoric times.

# Hidden histories

Most discussions involving weaving and programming refer to the development of Jacquard looms as the first computers. There are less well told, more international and more interesting connection points, such as Lisa Nakamura’s work researching the involvement of the Navajo women who designed Fairchild semiconductors' first integrated circuits [@nakamuraIndigenousCircuits2014]. The company directly referenced the similarity of the traditional weaving designs to electronic junctions and transistors, and used many images of Navajo designs in their company branding material.  

Is there some way we can unpick the threads of textile history in the central processing units of our devices today? Each processor has a defined set of instructions that it can execute - each instruction is represented by a physical circuit of transistors built for the job, so each instruction represents a considerable expense that needs to be minimised. Something common to nearly all processors is that the number of instructions that treat data as numerical value (e.g. plus, minus, compare) is outnumbered by a large degree by the instructions that treat data as a pattern (shifts, rotates & “bitwise operations”).

We can make these normally hidden operations visible, exposing the nature of the patterns that lie at the deepest levels of computation, as entirely physical processes. As they are governed by the same rules as everything else [information, as shown by Shannon in 1948, is limited by physics; @shannonMathematicalTheoryCommunication1963] we can show there is no magic cyberspace, just combinations of voltages or threads we interpret as patterns with meaning.

![The Z80 processor comes from a similar era as Fairchild’s integrated circuits, and was a foundational design to modern microprocessors. Here we display the contents of its register memory after every microcode instruction of two multiplies of 16 bit numbers to uncover the patterns created.](images/sinclair.png){ width=80%}

# Ancient patterns

During the workshop we explored replicating patterns from the Iron age (such as the Hallstatt textile) as well as Viking societies. We previewed them in a simulation built for the workshop which included its own code representation of tablet weaving movements, and tried out different variations to understand the logic of tablet weaving. The following section brings together some reflections from workshop participants.


![Selection of ancient Iron age and Viking tablet weaves](images/ironage.png){ width=80%}


![Zoom screenshot of the workshop](images/jitsi.jpg){ width=80% }

# Participants’ Reflections on the Workshop

Participants of the tablet weaving workshop each produced unique weaving patterns, some of which are shown in Fig.6. The participants were surveyed four months after the workshop, in order to gather longer term responses to the activity. The survey respondents represented below are Joana Chicau [JC], Flor de Fuego [FdF], Timo Hoogland [TH], Eloi Isern [EI], Michael-Jon Mizra [MJM], Iván Paz [IP], Roger Pibernat [RP], and Lizzie Wilson [LW]. We gave four prompts in the survey, which were as follows:

* How was your experience with the tablet weaving workshop? What was complex, what was simple? How does this compare or contrast from your experience with code?
* The following is an from excerpt Joanne Armitage's paper "Spaces to Fail in: Negotiating Gender, Community and Technology in Algorave".

  "For some, code emerges as a way of dealing with or organising life, for others code allows an expression of self, or a way of manipulating lived experiences and speaking back to them creatively. One person interviewed spoke about code as a way of working through their daily life, adding structures to it and providing functions for being. These lived patterns merge with their daydreams and expressions of colour and geometry to form her live coded visuals."
 
  How does this relate to your life? Can you share an experience that compares or contrasts with it?
* Do you enjoy a pattern-y craft or other pattern-y activity? E.g. weaving, braiding, origami, juggling, etc.. If so, what does live coding and this activity give you that compares, and where do they diverge?
* Have you thought about the workshop in the last few months, and if so what about it has stayed with you? Any influences on your thinking or makings?

 For the full set of participant responses, see the online repository for this paper, available at [gitlab.com/algopattern/patterns-in-deep-time](https://gitlab.com/algopattern/patterns-in-deep-time). Within those responses, we found emerging themes from their answers about a variety of reflections that they shared.
 
![](images/emergent-1.jpg){ width=40% }\ ![](images/emergent-2.jpg){ width=40% }\
![](images/emergent-3.jpg){ width=40% }\ ![](images/emergent-4.jpg){ width=40% }\
Figure 6: Emergent patterns from the participants’ weave. Image credit: Timo Hoogland

## Materiality / Physicality

Perhaps the most apparent difference between these practices is that live coders generally work only with code, rather than directly with material as with tablet weaving. However, we can say that there is always more in the ‘output’ of live coding, whether music, choreography or something else, than in the notation and rules for generating that output. This complexities of material became particularly apparent when working with threads: 

> "I found more complexity in controlling the materiality of the "wool" than remembering the movements. The weaving algorithms were clear in my head, but knowing the right tension, the right pressure and where to stop pulling was difficult at the beginning. With the successive repetitions the movements felt more natural." [IP].

In this answer the participant notes how the distinction between the cognitive processes and physical expression of these lead to some initial tensions for the participants. This was apparent for live coders, whose medium of expression - whilst still physical - relies heavily on cognitive processes. Live coding music has even been referred to as “the antithesis of physical musicianship” [@nilson2007live]. Despite some initial struggles perhaps, a few of the participants noted the appeal of this “hands-on” approach at the workshop, where they became absorbed into the repetitive movements, making space for focussed creativity:

> “I found the "hands-on" and movement focused character of weaving a smooth way of engaging in pattern making. For me muscle memory helps me a lot in making …over time, it became intuitive and fairly quickly I managed to improvise new patterns and explore more interesting combinations”[JC]

> “There was something quite enchanting about working with your hands and watching the patterns begin to appear” [LW] 

> “I enjoy the fact that.. making things in the moment, getting real time feedback from what you are making and not really being able to undo” [TH]

Philosophers, social theorists and anthropologists have all spoken of the new reality that we inhabit in the twenty-first century due to the vast expansion of digital technologies, and that the digital era is incontestably new. However, viewed from another perspective, perhaps it can also be thought of as less of a colossal leap from the physical to digital eras. Humans have always had an urge to keep their hands busy, and this is perhaps one of the reasons heritage practices like weaving, spinning and knitting were so culturally important. Typing on a keyboard, viewed through this lens, can be thought of as a natural progression of human behaviour. 

The progression of materiality from human-material to human-machine embodies the demarcation of the physical to digital progression, but weaving exists as an intermediary, where the human is in close contact with both the fabric and the machine. In weaving, "bits" are manipulated in real-time whilst in coding the abstraction of bits are manipulated through language, and by extension typing on a keyboard. By grounding live coders in this materiality, we hope this regression through human history allows them to make connections in how human-material loops and human-computer loops differ (e.g. in perceiving output and shifting behaviour).     

## Visualising algorithms

A few of the participants reflected on how the workshop led them to contextualise algorithms in visual terms. Visualisation can be understood to leverage the visual system and augment human intelligence as a way to understand abstract processes [@engelbart1962augmenting]. Indeed, algorithmic practice has many connections with spatial processes or abstractions that might require a strong sense of cognitive visualisation process, and especially live coding languages with a more functional approach. For example it often requires an understanding of ideas from geometry e.g., rotations, shifts, iterations; or linear algebra e.g. matrices or larger abstract structures and transition probabilities. One of the participants made the connection with how they use visualisation within mathematics, but drew a distinction between their experience of mathematical visualisation and what they were experiencing with the weaving:

> “I loved the conscious experience of following an algorithm, understanding it to the point that I can almost predict the result of a small variation, this has offered me a different experience of visualizing the algorithms that I normally use in maths, as if the process that they described had a more material presence in the physical time and space.” [IP]

> "I feel that the process of abstracting a concept is a process of gradual reduction. To atomise in this way grants one the gift of microscopic analyses. I also think this expands outwards, with the generalising description of systems, which conversely implores one to analyse at the macroscopic level. I can also therefore relate to the experience of the artist who found inspiration in their day dreams about patterns - once one starts thinking in this way, the world seems to respond in kind." [MJM]

The experience the participant is noting is how visualisation that usually takes place as a cognitive process becomes a physical one. This became especially apparent when the relationship between the algorithm and physical space became unified. Other participants experienced this unification between the physical and cognitive worlds and found this became clearer as the workshop progressed:

> “I also liked the newness, the mapping between what I was doing physically and what was coming out of the weave wasn’t always clear at first, but the more I navigated through the weave the more things started to become apparent.” [FDF]

> "..Trying out different rotations of the tablets and repeating my randomly thought-of algorithms to see what the pattern is that emerges over time. In some ways it fits my approach to programming music and visuals, where I can have an idea of an algorithm I would like to explore, starting with the “what if…?” question, and then see what happens from there over time" [TH]

## Errors/Expectations/Surprise Satisfaction/Fulfilment

Error is a common, and oftentimes celebrated, feature of live coding performance. One viewpoint of error is the divergence of the observed output and its intended value. If we frame error in this way, it can also be a source for providing creative impetus, if the unexpected provides us with surprise and/or fulfilment. As it happens in live coding performance, where missing a coma or writing an extra digit is a frequently occurring failure [@knotts2020live; @roberts2018tensions], errors were present while weaving, maybe twisting in the “wrong” direction or not applying the “right tension”. Errors contrast with what we had in mind, what we expect, and the results can surprise us in different ways. As in the practice of live coding, the live weaving action makes it easy to make mistakes, but those mistakes allow us to open up new avenues of exploration.

Some of the live coders who participated in the workshop compared the ways in which they encountered error in live coding with how they were experiencing it in the weaving workshop:

> "For me the “trial-and-error” approach worked pretty well. When programming music, I can make an educated guess on what I can expect to happen, while with the tablet weaving this was not so much the case since I was completely new to it. This resulted in some interesting surprises of patterns that came out." [TH].

> "As with coding, some complexity of the system began to arise when errors started to occur. It was relatively easy to undo sometimes, but there did seem times when small perturbations from what the instructor was doing felt that it shifted the outcome quite far. As with coding though, this did sometimes produce surprising and unexpected results that forced me to engage creatively with the weaving process." [LW]

> "... 'what is a pattern' is a question which positions itself between two poles; complexity/noise and simplicity/periodicity. These poles influence my approach to sound, where one can approach the construction of complex waveforms by the summation of simple wave forms, or one can construct wave forms through the use of stochastic processes.  I am also intrigued by the human capacity to recognise patterns, and how we exploit our limited bandwidth to create pseudo-random functions. And this leads me to wonder about the nature of true randomness, whether it is obtainable, and what does it mean for these two poles to exist in a universe that is both probabilistic and deterministic." [MJM]

> "The patterns that came out were really surprising, and it did feel a bit like getting unexpected results from code. [RP]"

The notion of fulfilment from surprise is well researched within the
context of the aesthetic experience of music. From a music-analytical
standpoint, it has been argued that the creation and subsequent
confirmation or violation of expectations is essential to aesthetic
experience and the musical communication of emotion and meaning
[@narmourAnalysisCognitionBasic1990]. David
@huronSweetAnticipationMusic2008 discusses what gives anticipation or
surprise their distinctive phenomenological characters, and also how
enforcing repetitions builds an expectation in the listener, and the
subsequent violation of these expectations elicits a physical
response. For others, they made note of what they found fulfilling in
this task:
 
> "The complex results, out of simple pattern-moves, were really satisfying. Maybe what I like about code is that it allows me to twist logic into poetic ways, which probably could be called a means of expression." [RP]

> "I try to look for code as an expressive tool for communicating and connecting with different disciplines." [FdF]

> "For someone like me, who does not come from a computer background, it was amazing to see the possibilities that opened up when working with the loom."  [EI]

## Patterns passed on, conveying meanings
 
Patterns are polysemic, as are melodies or fabric patterns. They are read, felt and interpreted in different ways. Sharing ideas, such as selecting a favourite pattern, among the participants visualises the different ways we interpret patterns by giving us perspective of others' experience. 

> "The exercise when people were asked to choose their favourite pattern and then pass it on to the next person to code in their own preferred language / software was interesting. That stayed with me, this idea of a collective string within which patterns are passed on 'hand-in-hand' , reinterpreted and creating a lineage of patterns." [JC]   	

> "I remember Dave saying something like this was a message that had thousands of years distance. And the idea of a weaving as a message, which I already somehow was aware because in Argentina we have that kind of idea with traditional weaving." [FdF]

> "I really liked the accompanying computational representation that was going on, and tried to do a code representation myself to help try and parse what was happening...  I also liked looking at the examples and seeing the way different cultures had their own representations of pattern that convey different meanings. It made me think about how music also conveys cultural meaning, and I wondered if there was any way of connecting these ideas of representation to musical representations (e.g. scores)." [LW]

![](images/pattern.png){ width=60% }

# Conclusion

Tablet weaving is an action through which algorithmic processes materialise. As in code, errors and other unanticipated results happen, and can be explored to obtain new unexpected surprises. These unplanned experiences, especially present when trying new things, are intrinsic to both weaving and coding. Weaving connects the physical materiality of the weaved patterns with their immaterial algorithmic descriptions. It allows us to visualise the algorithmic processes that describe the instructions throughout which they emerge. Weaving together in a group, following the same instructions, the same patterns, and sharing the experiences give us perspective of our relationship with the resulting patterns when comparing our experience with that of others. 
The central discussions on the workshop, as it was attended mainly by live coders, revolved around algorithms, time, error, repetition, and codification. But also about the way we interpret patterns such as rhythm, regularity, and how easy it is for us to recognize or perceive a pattern, i.e. the limits of our spatial and temporal perception. It is interesting that these ideas (descriptions) match the ways in which we describe the material and immaterial aspects of patterns.

The examples included in the simulator, ranging  from Egyptian to Viking,  added an extra layer to the different dimensions of the patterns: the way different cultures had their own representations of patterns (sometimes closely related) that convey different meanings.

# Acknowledgments

This work was part-funded by UKRI Future Leaders Fellowship [grantnumber MR/V025260/1], and by Creative Europe via the on-the-fly project. Additionally, Wilson’s contributions were supported by EPSRC and AHRC under the EP/L01632X/1 (Centre for Doctoral Training in Media and Arts Technology)

# References 
